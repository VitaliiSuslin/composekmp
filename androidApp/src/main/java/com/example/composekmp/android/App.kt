package com.example.composekmp.android

import android.app.Application
import com.example.composekmp.di.AppKoinModule
import com.example.composekmp.di.SharedInjector
import com.example.composekmp.getPlatform
import com.example.composekmp.sqldelight.appDataBase

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        AppKoinModule.init(this)
    }

    override fun onTerminate() {
        appDataBase().closeConnection()
        super.onTerminate()
    }
}