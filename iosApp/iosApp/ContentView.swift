import SwiftUI
import shared

struct ContentView: View {
	var body: some View {
        Group {
            ZStack{
                ComposeController()
            }
        }
	}
}

struct ComposeController : UIViewControllerRepresentable {
    
    func makeUIViewController(context: Context) -> some UIViewController {
        BackgroundCrashWorkaroundController()
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
           uiViewController.view.setNeedsLayout()
    }
}

class BackgroundCrashWorkaroundController: UIViewController {
    
    let composeController: UIViewController
    
    init() {
        self.composeController = Main_iosKt.MainViewController()
        self.composeController.modalPresentationStyle = .fullScreen
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if composeController.parent == nil {
            composeController.view.frame = view.bounds
            addChild(composeController)
            view.addSubview(composeController.view)
            composeController.didMove(toParent: self)
        }
    }
}
