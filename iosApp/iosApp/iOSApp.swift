//import SwiftUI

//@main
//struct iOSApp: App {
//	var body: some Scene {
//		WindowGroup {
//            ContentView()
//		}
//	}
//}

import UIKit
import shared

@UIApplicationMain
class iOSApp: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
            window = UIWindow(frame: UIScreen.main.bounds)
            let mainViewController = Main_iosKt.MainViewController()
            mainViewController.modalPresentationStyle = .overFullScreen
            window?.rootViewController = mainViewController
            window?.makeKeyAndVisible()
            return true
        }
}
