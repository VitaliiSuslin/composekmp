package com.example.composekmp.sqldelight

import app.cash.sqldelight.driver.native.NativeSqliteDriver
import com.example.composekmp.db.KMPComposeDatabase


internal class IOsSqlDelight : SQLDelightPlatform {
    //TODO think about getting platform driver, not all KMPComposeDatabase instead
    private val driver = NativeSqliteDriver(KMPComposeDatabase.Schema, SQLDelightConstant.DB_NAME)

    override fun database(): KMPComposeDatabase {
        return KMPComposeDatabase(driver)
    }

    override fun closeConnection() {
        driver.close()
    }
}

internal actual fun appDataBase(): SQLDelightPlatform = IOsSqlDelight()
