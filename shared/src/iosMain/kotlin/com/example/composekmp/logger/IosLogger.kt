package com.example.composekmp.logger

import com.example.composekmp.util.core.util.CustomLogger
import com.example.composekmp.util.core.util.Level
import platform.Foundation.NSLog

//Use native ios native logger
class IosLogger : CustomLogger {
    override fun log(throwable: Throwable) {
        NSLog("%s", "${throwable.message}")
    }

    override fun log(level: Level, throwable: Throwable) {
        NSLog("%s", "[${level.name}] message: ${throwable.message}")
    }

    override fun log(level: Level, throwable: Throwable, vararg message: String) {
        NSLog("%s", "[${level.name}] message: ${throwable.message}")
        NSLog("%s", message[0])
    }

    override fun log(level: Level, vararg message: String) {
        NSLog("%s", message[0])
    }
}