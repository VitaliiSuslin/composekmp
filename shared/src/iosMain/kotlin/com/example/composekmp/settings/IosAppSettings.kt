package com.example.composekmp.settings

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow

internal class IosAppSettings : AppSettings {

    private val appStateChoice = MutableStateFlow(AppDb.REALM)

    override suspend fun setAppDbChoice(appDb: AppDb) {
        appStateChoice.emit(appDb)
    }

    override fun appDbChoice(): AppDb {
        return appStateChoice.value
    }

    override fun observeAppDbChoice(): Flow<AppDb> = appStateChoice.asStateFlow()
}