package com.example.composekmp.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Application
import com.example.composekmp.di.UiInjector
import com.example.composekmp.util.core.util.ActionWrapper
import com.example.composekmp.view.Base
import com.example.composekmp.view.BaseBigX3
import com.example.composekmp.view.BaseSmallX1
import com.example.composekmp.view.BaseSmallX2
import com.example.composekmp.view.Dialog
import com.example.composekmp.view.MyApplicationTheme
import com.example.composekmp.view.SimpleTextField
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import platform.UIKit.UIViewController

fun MainViewController(): UIViewController = Application("ComposeKMP") {
    val mainUiProcessor = UiInjector.mainProcessor
    MyApplicationTheme {
        Surface(
            modifier = Modifier.fillMaxSize()
        ) {
            MainUi(modifier = Modifier.fillMaxSize().padding(top = 34.dp))
        }

        ObserveDialog(mainUiProcessor.showingDialogState)
        ObserveMainEvent(mainUiProcessor.action)
        ObserveMainMessage(mainUiProcessor.message)
    }
}


@Composable
private fun ObserveDialog(dialogState: StateFlow<DialogUiState>) {
    val dialogUiState = dialogState.collectAsState().value
    if (dialogUiState.isShowDialog) {
        NoteDialog(dialogUiState)
    }
}

@Composable
private fun ObserveMainEvent(action: Flow<ActionWrapper<MainAction>>) {
    action.collectAsState(ActionWrapper.Empty()).value.getContentIfNotConsumed()?.let {
    }
}

@Composable
private fun ObserveMainMessage(action: Flow<ActionWrapper<MainMessage>>) {
    action.collectAsState(ActionWrapper.Empty()).value.getContentIfNotConsumed()?.let {
        when (it) {
            is MainMessage.ShowMessagePopUp -> {
                Column(
                    modifier = Modifier.fillMaxSize().padding(bottom = BaseBigX3),
                    verticalArrangement = Arrangement.Bottom,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Card(
                        elevation = 4.dp,
                        shape = RoundedCornerShape(BaseBigX3),
                        backgroundColor = MaterialTheme.colors.secondary
                    ) {
                        Text(
                            text = it.message,
                            modifier = Modifier.padding(horizontal = Base, vertical = BaseSmallX2),
                            color = Color.White
                        )
                    }
                }
            }
        }
    }
}


@Composable
private fun NoteDialog(
    dialogUiState: DialogUiState
) {
    val titleState = remember { mutableStateOf(dialogUiState.noteInfo?.title ?: "") }
    val titleError = remember { mutableStateOf<String?>(null) }

    val textState = remember { mutableStateOf(dialogUiState.noteInfo?.text ?: "") }
    val textError = remember { mutableStateOf<String?>(null) }

    Dialog(
        dismiss = {
            dialogUiState.setShowDialog(false)
        }
    ) {
        Surface(
            modifier = Modifier.padding(start = Base, end = Base),
            color = MaterialTheme.colors.background,
            shape = RoundedCornerShape(BaseSmallX2),
        ) {
            Column(modifier = Modifier.padding(BaseSmallX2)) {
                Spacer(modifier = Modifier.height(Base))
                SimpleTextField(
                    getValue = { titleState.value },
                    label = { "Title" },
                    onTextChanged = {
                        titleState.value = it
                        titleError.value?.let {
                            titleError.value = null
                        }
                    },
                    onErrorChanged = { titleError.value }
                )
                Spacer(modifier = Modifier.height(BaseSmallX1))
                SimpleTextField(
                    getValue = { textState.value },
                    label = { "Text" },
                    onTextChanged = {
                        textState.value = it
                        textError.value?.let {
                            textError.value = null
                        }
                    },
                    onErrorChanged = { textError.value }
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(Base),
                    horizontalArrangement = Arrangement.End
                ) {
                    Button(onClick = {
                        val newTitle = titleState.value
                        if (newTitle.isBlank()) {
                            titleError.value = "Empty field!"
                        }
                        val newText = textState.value
                        if (newText.isBlank()) {
                            textError.value = "Empty field!"
                        }

                        if (titleError.value == null && textError.value == null) {
                            dialogUiState.onSave(newTitle, newText)
                            dialogUiState.setShowDialog(false)
                        }

                    }) {
                        Text(text = "Save")
                    }
                    Spacer(modifier = Modifier.width(BaseSmallX1))
                    Button(onClick = {
                        dialogUiState.setShowDialog(false)
                    }) {
                        Text(text = "Cancel")
                    }
                    Spacer(modifier = Modifier.width(BaseSmallX1))
                }
            }
        }
    }
}