package com.example.composekmp

import com.example.composekmp.logger.IosLogger
import com.example.composekmp.settings.AppSettings
import com.example.composekmp.settings.IosAppSettings
import com.example.composekmp.util.core.util.CustomLogger
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import platform.Foundation.NSUUID
import platform.UIKit.UIDevice

class IOSPlatform : Platform {
    override val name: String =
        UIDevice.currentDevice.systemName() + " " + UIDevice.currentDevice.systemVersion

    //    override val dispatcherIO: CoroutineDispatcher get() = newFixedThreadPoolContext(50, "io")
    override val dispatcherIO: CoroutineDispatcher get() = Dispatchers.Default

    override val dispatcherMain: CoroutineDispatcher get() = Dispatchers.Main

    override val platformType: PlatformType = PlatformType.IOS

    override fun generateUuid(): String {
        return NSUUID.UUID().UUIDString()
    }

    override fun appSettings(): AppSettings {
        return IosAppSettings()
    }

    override fun logger(): CustomLogger = IosLogger()
}

actual fun getPlatform(): Platform = IOSPlatform()