package com.example.composekmp.realm.model

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.RealmUUID
import io.realm.kotlin.types.annotations.PrimaryKey

class Note : RealmObject {
    @PrimaryKey
    var _id: RealmUUID = RealmUUID.random()
    var title: String = String()
    var text: String = String()
    var event: String = "default"
}