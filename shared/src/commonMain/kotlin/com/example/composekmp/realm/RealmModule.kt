package com.example.composekmp.realm

import com.example.composekmp.realm.model.Note
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import io.realm.kotlin.log.LogLevel
import io.realm.kotlin.mongodb.App
import io.realm.kotlin.mongodb.AppConfiguration
import io.realm.kotlin.mongodb.Credentials
import io.realm.kotlin.mongodb.sync.SyncConfiguration


private val realmObjects = setOf(Note::class)

class RealmModule {
    companion object {
        private const val realmId = "emojigarden-xfhmw"
    }

    private val realmApp by lazy {
        val appConfig = AppConfiguration.Builder(appId = realmId).log(LogLevel.ALL).build()
        App.create(appConfig)
    }

    private val _realm: Realm by lazy {
        val config = RealmConfiguration.create(realmObjects)
        Realm.open(config)
    }

    suspend fun signUp(email: String, password: String) {
        realmApp.emailPasswordAuth.registerUser(email, password)
    }

    suspend fun signIn(email: String, password: String) {
        realmApp.login(Credentials.emailPassword(email, password))
    }

    fun getCurrentUser() = realmApp.currentUser
    fun getRealm() = _realm

    suspend fun signOut() {
        realmApp.currentUser?.logOut()
    }

//    private lateinit var realmConfiguration: RealmConfiguration
//    private lateinit var realm2: Realm
//
//    fun invokeRealmAction(block: Realm.() -> Unit) {
//        val actionRealm = Realm.open(getRealmConfiguration())
//        block.invoke(actionRealm)
//        actionRealm.close()
//    }
//
//    fun observableRealm(): Realm {
//        return if (this::realm2.isInitialized) {
//            realm2.close()
//            realm2 = Realm.open(getRealmConfiguration())
//            realm2
//        } else {
//            realm2 = Realm.open(getRealmConfiguration())
//            realm2
//        }
//    }
//
//    private fun getRealmConfiguration(): RealmConfiguration {
//        return if (this::realmConfiguration.isInitialized) {
//            realmConfiguration
//        } else RealmConfiguration.create(realmObjects)
//    }
//
//    private val realm by lazy {
//        val user = getCurrentUser()!!
//
//        val config = SyncConfiguration.Builder(user, schema = realmObjects).schemaVersion(1)
//            .initialSubscriptions { _realm ->
//                add(_realm.query(clazz = Note::class), updateExisting = true)
//            }
//            .waitForInitialRemoteData()
//            .build()
//        Realm.open(config)
//    }
}