package com.example.composekmp.util.core.ui

enum class ScreenState {
    LOADING, CONTENT, PLACEHOLDER
}