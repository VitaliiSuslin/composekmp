package com.example.composekmp.util.util

import com.example.composekmp.getPlatform
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel

abstract class KMPViewModel {

    protected val viewModelScope: CoroutineScope = CoroutineScope(getPlatform().dispatcherMain)

    fun onClose() {
        viewModelScope.cancel()
    }
}