package com.example.composekmp.util.core.util

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.receiveAsFlow

fun <T> Channel<ActionWrapper<T>>.receiveAsCleanableActionFlow(): Flow<ActionWrapper<T>> = flow {
    this@receiveAsCleanableActionFlow.receiveAsFlow().collect {
        this.emit(it)
        delay(100)
        this.emit(ActionWrapper.Empty())
    }
}

fun <T> Channel<ActionWrapper.Message<T>>.receiveMessagesCleanableFlow(): Flow<ActionWrapper<T>> =
    flow {
        this@receiveMessagesCleanableFlow.receiveAsFlow().collect {
            this.emit(it)
            delay(it.duration)
            this.emit(ActionWrapper.Empty())
        }
    }

const val DEFAULT_FLOW_REPLY = 1
