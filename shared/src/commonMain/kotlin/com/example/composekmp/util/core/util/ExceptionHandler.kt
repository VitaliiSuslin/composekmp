package com.example.composekmp.util.core.util

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.channels.Channel


fun viewModelExceptionHandler(
    logger: CustomLogger? = null,
    errorChannel: Channel<FailureWrapper>? = null
): CoroutineExceptionHandler {
    return CoroutineExceptionHandler { _, throwable ->
        logger?.log(Level.ERROR, throwable, "Exception handler")
        errorChannel?.trySend(FailureWrapper(throwable))
    }
}