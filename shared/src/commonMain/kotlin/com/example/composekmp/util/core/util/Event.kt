package com.example.composekmp.util.core.util

import com.example.composekmp.getPlatform

sealed class ActionWrapper<out T>(
    private val id: String, private val arguments: T? = null
) {

    class Empty<T> : ActionWrapper<T>(getPlatform().generateUuid())
    data class Action<T>(val data: T) : ActionWrapper<T>(getPlatform().generateUuid(), data)
    data class Message<T>(val data: T, val duration: Long) :
        ActionWrapper<T>(getPlatform().generateUuid(), data)

    private var hasBeenConsumed = false

    fun getContentIfNotConsumed(): T? {
        return if (hasBeenConsumed) {
            null
        } else {
            hasBeenConsumed = true
            arguments
        }
    }
}

data class FailureWrapper(val throwable: Throwable? = null) {

    private var throwableHasBeenConsumed = false

    fun getThrowableIfNotConsumed(): Throwable? {
        return if (throwableHasBeenConsumed) {
            null
        } else {
            throwableHasBeenConsumed = true
            throwable
        }
    }
}