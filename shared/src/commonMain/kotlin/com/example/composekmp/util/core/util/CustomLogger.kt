package com.example.composekmp.util.core.util

interface CustomLogger {
    fun log(throwable: Throwable)
    fun log(level: Level, throwable: Throwable)
    fun log(level: Level, throwable: Throwable, vararg message: String)
    fun log(level: Level, vararg message: String)
}

enum class Level { ERROR, WARNING, DEBUG, INFORMATION, VERBOSE }