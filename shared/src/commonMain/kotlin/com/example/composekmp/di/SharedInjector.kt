package com.example.composekmp.di

import com.example.composekmp.auth.repository.AuthRepository
import com.example.composekmp.auth.repository.AuthRepositoryImpl
import com.example.composekmp.auth.viewmodel.delegate.SignInViewModelDelegate
import com.example.composekmp.auth.viewmodel.delegate.SignInViewModelDelegateImpl
import com.example.composekmp.auth.viewmodel.delegate.SignUpViewModelDelegate
import com.example.composekmp.auth.viewmodel.delegate.SignUpViewModelDelegateImpl
import com.example.composekmp.auth.viewmodel.delegate.SplashViewModelDelegate
import com.example.composekmp.auth.viewmodel.delegate.SplashViewModelDelegateImpl
import com.example.composekmp.getPlatform
import com.example.composekmp.note.repository.NoteRepositoryResolver
import com.example.composekmp.note.viewmodel.delegate.NoteViewModelDelegate
import com.example.composekmp.note.viewmodel.delegate.NoteViewModelDelegateImpl
import com.example.composekmp.realm.RealmModule
import com.example.composekmp.settings.AppSettings

object SharedInjector {

    //RealmModule
    val realmModule by lazy { RealmModule() }

    //App settings

    val appSettings: AppSettings by lazy { getPlatform().appSettings() }

    //Repository
    private val authRepository: AuthRepository by lazy { AuthRepositoryImpl(realmModule) }
    private val noteRepository by lazy { NoteRepositoryResolver(realmModule, appSettings) }

    //Delegates
    val splashViewModelDelegate: SplashViewModelDelegate
        get() = SplashViewModelDelegateImpl(
            authRepository
        )

    val signInViewModelDelegate: SignInViewModelDelegate
        get() = SignInViewModelDelegateImpl(
            authRepository,
            appSettings
        )

    val signUpViewModelDelegate: SignUpViewModelDelegate
        get() = SignUpViewModelDelegateImpl(
            authRepository
        )

    val noteViewModelDelegate: NoteViewModelDelegate
        get() = NoteViewModelDelegateImpl(
            authRepository,
            noteRepository
        )
}