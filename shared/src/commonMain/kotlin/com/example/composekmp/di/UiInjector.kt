package com.example.composekmp.di

import com.example.composekmp.ui.MainUiProcessor
import com.example.composekmp.ui.ViewModelStore

object UiInjector {
    val mainProcessor: MainUiProcessor by lazy { MainUiProcessor() }
    val viewModelStore: ViewModelStore by lazy { ViewModelStore() }
}