package com.example.composekmp.ui

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import com.example.composekmp.di.UiInjector
import com.example.composekmp.getPlatform
import com.example.composekmp.ui.auth.SignInUi
import com.example.composekmp.ui.auth.SignUpUi
import com.example.composekmp.ui.auth.SplashUi
import com.example.composekmp.ui.auth.viewmodel.SignInViewModel
import com.example.composekmp.ui.auth.viewmodel.SignUpViewModel
import com.example.composekmp.ui.auth.viewmodel.SplashViewModel
import com.example.composekmp.ui.chooser.ChooserUi
import com.example.composekmp.ui.note.NoteUi
import com.example.composekmp.ui.note.viewmodel.NoteViewModel
import com.example.composekmp.util.core.util.Level
import com.example.composekmp.view.CustomNavigation
import com.example.composekmp.view.CustomNavigationSettings
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow


@Composable
internal fun MainUi(
    modifier: Modifier = Modifier,
) {
    Surface(
        modifier = modifier,
        color = MaterialTheme.colors.background
    ) {

        val screen = remember { MutableStateFlow(Screen.Splash.route) }

        val navigation = remember {
            CustomNavigation(
                startDestination = screen.value,
                onNavigationListener = { closableRoute ->
                    UiInjector.viewModelStore.close(closableRoute)
                }
            ).apply {
                composable(route = Screen.Splash.route) {
                    screen.value = Screen.Splash.route
                }
                composable(route = Screen.SignIn.route) {
                    screen.value = Screen.SignIn.route
                }
                composable(route = Screen.SignUp.route) {
                    screen.value = Screen.SignUp.route
                }
                composable(route = Screen.Notes.route) {
                    screen.value = Screen.Notes.route
                }
                composable(route = Screen.Chooser.route) {
                    screen.value = Screen.Chooser.route
                }
            }
        }


        ScreenNavigate(
            screenState = screen,
            appNavController = navigation
        )
        navigation.start()
    }
}

@Composable
private fun ScreenNavigate(
    screenState: StateFlow<String>,
    appNavController: CustomNavigation,
) {
    val screen by screenState.collectAsState()
    getPlatform().logger().log(Level.ERROR, screen)
    when (screen) {
        Screen.Splash.route -> {
            SplashUi(
                UiInjector.mainProcessor,
                appNavController,
                UiInjector.viewModelStore.getViewModel(
                    Screen.Splash.route,
                    SplashViewModel()
                )
            )
        }
        Screen.SignIn.route -> {
            SignInUi(
                UiInjector.mainProcessor,
                appNavController,
                UiInjector.viewModelStore.getViewModel(
                    Screen.SignIn.route,
                    SignInViewModel()
                )
            )
        }
        Screen.SignUp.route -> {
            SignUpUi(
                UiInjector.mainProcessor,
                appNavController,
                UiInjector.viewModelStore.getViewModel(
                    Screen.SignUp.route,
                    SignUpViewModel()
                )
            )
        }
        Screen.Notes.route -> {
            NoteUi(
                UiInjector.mainProcessor,
                appNavController,
                UiInjector.viewModelStore.getViewModel(
                    Screen.Notes.route,
                    NoteViewModel()
                )
            )
        }
        Screen.Chooser.route -> ChooserUi(appNavController)
    }
}

sealed class Screen(val route: String) {
    object Splash : Screen("splash")
    object SignIn : Screen("sign_in")
    object SignUp : Screen("sign_up")
    object Notes : Screen("notes")
    object Chooser : Screen("chooser")
}

fun CustomNavigation.navigateToNoteScreen(popUpTo: String) =
    navigate(Screen.Notes.route) {
        CustomNavigationSettings().apply {
            this.popUpRoute = popUpTo
            this.inclusive = true
        }
    }


fun CustomNavigation.navigateToSignInScreen(popUpTo: String) =
    navigate(Screen.SignIn.route) {
        CustomNavigationSettings().apply {
            this.popUpRoute = popUpTo
            this.inclusive = true
        }
    }

fun CustomNavigation.navigateToSignUpScreen() = navigate(Screen.SignUp.route)

fun CustomNavigation.navigateToChooserScreen(popUpTo: String) = navigate(Screen.Chooser.route) {
    CustomNavigationSettings().apply {
        this.popUpRoute = popUpTo
        this.inclusive = true
    }
}