package com.example.composekmp.ui

import com.example.composekmp.util.util.KMPViewModel

class ViewModelStore {

    private val _viewModelStore = mutableMapOf<String, KMPViewModel>()

    fun <T> getViewModel(key: String, viewModel: KMPViewModel): T {
        return _viewModelStore.getViewModel(key, viewModel)
    }

    fun close(key: String) {
        _viewModelStore[key]?.onClose()
        _viewModelStore.remove(key)
    }

    fun closeAll() {
        _viewModelStore.values.forEach {
            it.onClose()
        }
    }

    private inline fun <reified T : KMPViewModel> MutableMap<String, KMPViewModel>.getViewModel(
        key: String,
        viewModel: KMPViewModel
    ): T {
        return getOrPut(key, defaultValue = { viewModel }) as T
    }
}