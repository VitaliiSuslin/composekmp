package com.example.composekmp.ui.auth.viewmodel

import com.example.composekmp.auth.viewmodel.delegate.SignUpViewModelDelegate
import com.example.composekmp.di.SharedInjector
import com.example.composekmp.util.util.KMPViewModel

internal class SignUpViewModel(
    val delegate: SignUpViewModelDelegate = SharedInjector.signUpViewModelDelegate
) : KMPViewModel(), SignUpViewModelDelegate by delegate {

    init {
        delegate.initialize(viewModelScope)
    }
}