package com.example.composekmp.ui.auth.viewmodel

import com.example.composekmp.auth.viewmodel.delegate.SignInUiEvent
import com.example.composekmp.auth.viewmodel.delegate.SignInViewModelDelegate
import com.example.composekmp.di.SharedInjector
import com.example.composekmp.util.util.KMPViewModel

internal class SignInViewModel(
    delegate: SignInViewModelDelegate = SharedInjector.signInViewModelDelegate
) : KMPViewModel(), SignInViewModelDelegate by delegate {

    init {
        delegate.initialize(viewModelScope)
        delegate.processUiEvent(SignInUiEvent.Start)
    }
}