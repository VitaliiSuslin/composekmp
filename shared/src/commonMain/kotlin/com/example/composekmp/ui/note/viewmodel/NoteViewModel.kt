package com.example.composekmp.ui.note.viewmodel

import com.example.composekmp.di.SharedInjector
import com.example.composekmp.note.viewmodel.delegate.NoteUiEvent
import com.example.composekmp.note.viewmodel.delegate.NoteViewModelDelegate
import com.example.composekmp.util.util.KMPViewModel

open class NoteViewModel(
    delegate: NoteViewModelDelegate = SharedInjector.noteViewModelDelegate
) : KMPViewModel(), NoteViewModelDelegate by delegate {

    init {
        delegate.initialize(viewModelScope)
        delegate.processUiEvent(NoteUiEvent.Start)
    }
}