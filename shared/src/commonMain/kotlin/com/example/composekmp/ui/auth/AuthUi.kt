package com.example.composekmp.ui.auth

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Checkbox
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.text.font.FontWeight
import com.example.composekmp.Greeting
import com.example.composekmp.PlatformType
import com.example.composekmp.auth.viewmodel.delegate.SignInActions
import com.example.composekmp.auth.viewmodel.delegate.SignInUiEvent
import com.example.composekmp.auth.viewmodel.delegate.SignInViewModelDelegate
import com.example.composekmp.auth.viewmodel.delegate.SignUpAction
import com.example.composekmp.auth.viewmodel.delegate.SignUpUiEvent
import com.example.composekmp.auth.viewmodel.delegate.SignUpViewModelDelegate
import com.example.composekmp.auth.viewmodel.delegate.SplashAction
import com.example.composekmp.auth.viewmodel.delegate.SplashViewModelDelegate
import com.example.composekmp.getPlatform
import com.example.composekmp.settings.AppDb
import com.example.composekmp.ui.MainUiProcessor
import com.example.composekmp.ui.Screen
import com.example.composekmp.ui.navigateToChooserScreen
import com.example.composekmp.ui.navigateToNoteScreen
import com.example.composekmp.ui.navigateToSignUpScreen
import com.example.composekmp.util.core.util.FailureWrapper
import com.example.composekmp.view.Base
import com.example.composekmp.view.BaseBigX1
import com.example.composekmp.view.BaseSmallX2
import com.example.composekmp.view.CustomNavigation
import com.example.composekmp.view.PasswordTextField
import com.example.composekmp.view.SimpleProgress
import com.example.composekmp.view.SimpleTextField
import kotlinx.coroutines.flow.Flow

@Composable
internal fun SplashUi(
    mainUiProcessor: MainUiProcessor,
    navController: CustomNavigation,
    viewModel: SplashViewModelDelegate
) {
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Column {
            Text(text = Greeting().greet())
            Text(text = "Welcome to Compose KMP project")
            SimpleProgress()
        }
    }

    ObserveSplashActions(navController = navController, viewModel = viewModel)
    ObserveError(mainUiProcessor, viewModel.error)
}

@Composable
private fun ObserveSplashActions(
    navController: CustomNavigation,
    viewModel: SplashViewModelDelegate
) {
    viewModel.action.collectAsState(null).value?.getContentIfNotConsumed()?.let { actions ->
        when (actions) {
            SplashAction.OpenNotesScreen -> navController.navigateToNoteScreen(Screen.Splash.route)
            SplashAction.OpenChooserScreen -> navController.navigateToChooserScreen(Screen.Splash.route)
        }
    }
}


@Composable
internal fun SignInUi(
    mainUiProcessor: MainUiProcessor,
    navController: CustomNavigation,
    viewModel: SignInViewModelDelegate
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(Base)
            .verticalScroll(rememberScrollState()),
    ) {

        val isSystemProcessing by viewModel.isSystemProcessing.collectAsState()
        val email = remember { mutableStateOf(viewModel.email.value) }
        val emailError = remember { mutableStateOf(viewModel.emailError.value?.message) }
        SimpleTextField(getValue = { email.value },
            label = { "Email" },
            onTextChanged = {
                email.value = it
                emailError.value?.let {
                    emailError.value = null
                }
            },
            isEnable = { isSystemProcessing.not() },
            onErrorChanged = {
                emailError.value
            })
        Spacer(modifier = Modifier.height(BaseBigX1))
        val password = remember { mutableStateOf(viewModel.password.value) }
        val passwordError = remember { mutableStateOf(viewModel.passwordError.value?.message) }
        PasswordTextField(value = {
            password.value
        },
            label = { "Password" },
            onTextChanged = {
                password.value = it
                passwordError.value?.let {
                    passwordError.value = null
                }
            },
            isEnable = { isSystemProcessing.not() },
            onErrorChanged = {
                passwordError.value
            })
        Spacer(modifier = Modifier.height(BaseBigX1))
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(Base),
            onClick = {
                viewModel.processUiEvent(SignInUiEvent.EmailIsChanged(email.value))
                viewModel.processUiEvent(SignInUiEvent.PasswordIsChanged(password.value))
                viewModel.processUiEvent(SignInUiEvent.SignIn)

            },
            enabled = isSystemProcessing.not()
        ) {
            Text(text = "Sign in")
        }
        Button(
            onClick = {
                navController.navigateToSignUpScreen()
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(Base)
        ) {
            Text(
                text = "Create account!",
                fontWeight = FontWeight.Bold
            )
        }
        DbChooserSection(viewModel = viewModel)

        if (getPlatform().platformType == PlatformType.IOS) {
            Text(
                text = "Warning! Realm auth doesn't work well on iOS!",
                modifier = Modifier.padding(Base)
            )
        }
    }
    ObserveSignInActions(navController, viewModel)
    ObserveError(mainUiProcessor, viewModel.error)
}

@Composable
private fun DbChooserSection(modifier: Modifier = Modifier, viewModel: SignInViewModelDelegate) {
    Column(modifier = modifier) {
        val appDb by viewModel.dbState.collectAsState()
        Row(verticalAlignment = Alignment.CenterVertically) {
            Checkbox(checked = appDb == AppDb.REALM, onCheckedChange = {
                if (appDb != AppDb.REALM) {
                    viewModel.processUiEvent(SignInUiEvent.AppDbIsChanged(AppDb.REALM))
                }
            })
            Text(text = "Realm")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Checkbox(checked = appDb == AppDb.SQL_DELIGHT, onCheckedChange = {
                if (appDb != AppDb.SQL_DELIGHT) {
                    viewModel.processUiEvent(SignInUiEvent.AppDbIsChanged(AppDb.SQL_DELIGHT))
                }
            })
            Text(text = "SQLDelight")
        }
    }
}

@Composable
private fun ObserveSignInActions(
    navController: CustomNavigation,
    viewModel: SignInViewModelDelegate
) {
    viewModel.action.collectAsState(null).value?.getContentIfNotConsumed()?.let { actions ->
        when (actions) {
            SignInActions.OpenNotesScreen -> navController.navigateToNoteScreen(Screen.SignIn.route)
        }
    }
}


@Composable
internal fun SignUpUi(
    mainUiProcessor: MainUiProcessor,
    navController: CustomNavigation,
    viewModel: SignUpViewModelDelegate
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(Base)
            .verticalScroll(rememberScrollState()),
    ) {
        val isSystemProcessing by viewModel.isSystemProcessing.collectAsState()

        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            IconButton(
                onClick = { navController.popBackStack() }
            ) {
                Icon(
                    painter = rememberVectorPainter(image = Icons.Default.ArrowBack),
                    contentDescription = "Arrow back"
                )
            }

            Text(
                text = "Creating new account",
                modifier = Modifier.padding(start = BaseSmallX2, end = BaseSmallX2),
                fontWeight = FontWeight.SemiBold
            )
        }

        Spacer(modifier = Modifier.height(Base))
        val email = remember { mutableStateOf(viewModel.email.value) }
        val emailError by viewModel.emailError.collectAsState()
        SimpleTextField(getValue = { email.value },
            label = { "Email" },
            onTextChanged = {
                email.value = it
                emailError?.let {
                    viewModel.processUiEvent(SignUpUiEvent.RemoveEmailError)
                }
            },
            isEnable = { isSystemProcessing.not() },
            onErrorChanged = { emailError?.message })
        Spacer(modifier = Modifier.height(BaseBigX1))
        val password = remember { mutableStateOf(viewModel.password.value) }
        val passwordError by viewModel.passwordError.collectAsState()
        PasswordTextField(value = { password.value },
            label = { "Password" },
            onTextChanged = {
                password.value = it
                passwordError?.let {
                    viewModel.processUiEvent(SignUpUiEvent.RemovePasswordError)
                }
            },
            isEnable = { isSystemProcessing.not() },
            onErrorChanged = { passwordError?.message })
        Spacer(modifier = Modifier.height(BaseBigX1))
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(Base),
            onClick = {
                viewModel.processUiEvent(SignUpUiEvent.EmailIsChanged(email.value))
                viewModel.processUiEvent(SignUpUiEvent.PasswordIsChanged(password.value))
                viewModel.processUiEvent(SignUpUiEvent.SignUp)
            },
            enabled = isSystemProcessing.not()
        ) {
            Text(text = "Sign up")
        }
    }
    ObserveSignUpActions(navController, viewModel)
    ObserveError(mainUiProcessor, viewModel.error)
}

@Composable
private fun ObserveSignUpActions(
    navController: CustomNavigation,
    viewModel: SignUpViewModelDelegate
) {
    viewModel.action.collectAsState(null).value?.getContentIfNotConsumed()?.let { actions ->
        when (actions) {
            SignUpAction.OpenNotesScreen -> navController.navigateToNoteScreen(Screen.SignUp.route)
        }
    }
}

@Composable
private fun ObserveError(mainUiProcessor: MainUiProcessor, error: Flow<FailureWrapper>) {
    error.collectAsState(null).value?.getThrowableIfNotConsumed()?.let { throwable ->
        mainUiProcessor.showMessage("Error: ${throwable.message}")
    }
}