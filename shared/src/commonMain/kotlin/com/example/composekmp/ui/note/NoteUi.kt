package com.example.composekmp.ui.note

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Divider
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.ExitToApp
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import com.example.composekmp.note.model.CreateNote
import com.example.composekmp.note.model.NoteInfo
import com.example.composekmp.note.viewmodel.delegate.NoteAction
import com.example.composekmp.note.viewmodel.delegate.NoteUiEvent
import com.example.composekmp.note.viewmodel.delegate.NoteViewModelDelegate
import com.example.composekmp.ui.DialogUiState
import com.example.composekmp.ui.MainUiProcessor
import com.example.composekmp.ui.Screen
import com.example.composekmp.ui.navigateToChooserScreen
import com.example.composekmp.ui.navigateToSignInScreen
import com.example.composekmp.util.core.ui.ScreenState
import com.example.composekmp.view.Base
import com.example.composekmp.view.BaseBigX2
import com.example.composekmp.view.BaseSmallX1
import com.example.composekmp.view.BaseSmallX2
import com.example.composekmp.view.CustomNavigation
import com.example.composekmp.view.SimpleProgress


@Composable
internal fun NoteUi(
    mainUiProcessor: MainUiProcessor,
    navController: CustomNavigation,
    viewModel: NoteViewModelDelegate
) {
    Scaffold(modifier = Modifier.fillMaxSize(), topBar = {
        Top(viewModel = viewModel)
    }, floatingActionButton = {
        FloatingActionButton(onClick = {
            viewModel.processUiEvent(NoteUiEvent.OpenCreateNote)
        }) {
            Icon(
                painter = rememberVectorPainter(image = Icons.Default.Add),
                contentDescription = "Add note",
                tint = Color.White
            )
        }
    }) {
        ObserveActions(mainUiProcessor, viewModel = viewModel, navController = navController)
        ObserveError(mainUiProcessor, viewModel)

        Content(modifier = Modifier.padding(it), viewModel = viewModel)
    }
}


@Composable
private fun ObserveActions(
    mainUiProcessor: MainUiProcessor,
    viewModel: NoteViewModelDelegate,
    navController: CustomNavigation
) {
    viewModel.action.collectAsState(initial = null).value?.getContentIfNotConsumed()
        ?.let { action ->
            when (action) {
                NoteAction.MoteToSplash -> {
                    navController.navigateToChooserScreen(Screen.Notes.route)
                }
                NoteAction.NoteIsCreated -> {
                    mainUiProcessor.showMessage("Note is created!")
                }
                NoteAction.NoteIsDeleted -> {
                    mainUiProcessor.showMessage("Note is deleted!")
                }
                NoteAction.NoteIsUpdated -> {
                    mainUiProcessor.showMessage("Note is updated!")
                }
                is NoteAction.OpenEditNoteDialog -> {
                    ShowDialog(noteInfo = action.noteInfo, mainUiProcessor, viewModel)
                }
                NoteAction.OpenNoteCreateDialog -> {
                    ShowDialog(noteInfo = null, mainUiProcessor, viewModel)
                }
                NoteAction.OpenLogin -> navController.navigateToSignInScreen(Screen.Notes.route)
            }
        }
}

@Composable
private fun ShowDialog(
    noteInfo: NoteInfo? = null,
    mainUiProcessor: MainUiProcessor,
    viewModel: NoteViewModelDelegate,
) {

    if (noteInfo == null) {
        mainUiProcessor.showNoteDialog(
            DialogUiState(
                setShowDialog = { mainUiProcessor.showingDialog(it) },
                onSave = { title, text ->
                    viewModel.processUiEvent(
                        NoteUiEvent.AddNewNote(CreateNote(title, text))
                    )
                }
            ))
    } else {
        mainUiProcessor.showNoteDialog(
            DialogUiState(
                noteInfo,
                setShowDialog = { mainUiProcessor.showingDialog(it) },
                onSave = { title, text ->
                    viewModel.processUiEvent(
                        NoteUiEvent.UpdateNote(NoteInfo(noteInfo.id, title, text))
                    )
                }
            ))
    }
}

@Composable
private fun ObserveError(mainUiProcessor: MainUiProcessor, viewModel: NoteViewModelDelegate) {
    viewModel.error.collectAsState(initial = null).value?.getThrowableIfNotConsumed()
        ?.let { throwable ->
            mainUiProcessor.showMessage("Error: ${throwable.message}")
        }
}

@Composable
private fun Top(viewModel: NoteViewModelDelegate) {
    if (viewModel.isSearchMode.collectAsState().value) {
        TopAppBar {
            val rememberText = remember { mutableStateOf(String()) }
            val keyboard = LocalSoftwareKeyboardController.current
            TextField(
                value = rememberText.value,
                onValueChange = {
                    rememberText.value = it
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = BaseSmallX2, end = BaseBigX2)
                    .background(color = MaterialTheme.colors.primary),
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Search,
                    autoCorrect = false
                ),
                keyboardActions = KeyboardActions(
                    onSearch = {
                        keyboard?.hide()
                        viewModel.processUiEvent(NoteUiEvent.SearchQuery(rememberText.value))
                    }, onDone = {
                        keyboard?.hide()
                    }),
                leadingIcon = {
                    IconButton(onClick = {
                        keyboard?.hide()
                        viewModel.processUiEvent(NoteUiEvent.SearchQuery(rememberText.value))
                    }) {
                        Icon(
                            tint = MaterialTheme.colors.onPrimary,
                            painter = rememberVectorPainter(image = Icons.Default.Search),
                            contentDescription = "Search"
                        )
                    }
                },
                trailingIcon = {
                    if (rememberText.value.isNotBlank()) {
                        IconButton(onClick = { rememberText.value = String() }) {
                            Icon(
                                tint = MaterialTheme.colors.onPrimary,
                                painter = rememberVectorPainter(image = Icons.Default.Clear),
                                contentDescription = "Clear"
                            )
                        }
                    } else {
                        IconButton(onClick = { viewModel.processUiEvent(NoteUiEvent.CloseSearchMode) }) {
                            Icon(
                                tint = MaterialTheme.colors.onPrimary,
                                painter = rememberVectorPainter(image = Icons.Default.Close),
                                contentDescription = "Clear"
                            )
                        }
                    }
                }
            )
        }
    } else {
        TopAppBar(title = { Text(text = "Notes") }, actions = {
            IconButton(onClick = { viewModel.processUiEvent(NoteUiEvent.SearchMode) }) {
                Icon(
                    painter = rememberVectorPainter(image = Icons.Default.Search),
                    contentDescription = "Search"
                )
            }

            val isUserLoggedIn by viewModel.isUserLoggedIn.collectAsState()

            IconButton(onClick = { viewModel.processUiEvent(NoteUiEvent.LogOut) }) {
                Icon(
                    painter = rememberVectorPainter(image = Icons.Default.ExitToApp),
                    contentDescription = "Exit to chooser screen"
                )
            }

            if (isUserLoggedIn.not()) {
                IconButton(onClick = { viewModel.processUiEvent(NoteUiEvent.LogIn) }) {
                    Icon(
                        painter = rememberVectorPainter(image = Icons.Default.AccountCircle),
                        contentDescription = "Sign in"
                    )
                }
            }
        })
    }
}

@Composable
private fun Content(modifier: Modifier, viewModel: NoteViewModelDelegate) {
    Box(modifier = modifier.fillMaxSize()) {
        when (viewModel.screenState.collectAsState().value) {
            ScreenState.LOADING -> {
                SimpleProgress(modifier = Modifier.align(Alignment.Center))
            }
            ScreenState.CONTENT -> {
                List(modifier, viewModel)
            }
            ScreenState.PLACEHOLDER -> {
                Text(text = "List is empty!", modifier = Modifier.align(Alignment.Center))
            }
        }
    }
}

@Composable
private fun List(modifier: Modifier, viewModel: NoteViewModelDelegate) {
    val list = viewModel.notes.collectAsState().value
    LazyColumn(
        modifier = modifier.fillMaxSize(), state = rememberLazyListState()
    ) {
        items(items = list, key = { it.id }) {
            NoteItem(noteInfo = it, onItemClick = {
                viewModel.processUiEvent(NoteUiEvent.OpenUpdateNote(it))
            }, onDelete = {
                viewModel.processUiEvent(NoteUiEvent.DeleteNote(it))
            })
            Divider()
        }
    }
}

@Composable
private fun NoteItem(noteInfo: NoteInfo, onItemClick: () -> Unit, onDelete: () -> Unit) {
    Row(modifier = Modifier.fillMaxWidth().background(color = MaterialTheme.colors.background)
        .clickable { onItemClick() }) {
        Column(
            modifier = Modifier.padding(Base).weight(1f)
        ) {
            Text(text = noteInfo.title, fontWeight = FontWeight.Bold)
            Spacer(modifier = Modifier.height(BaseSmallX1))
            Text(text = noteInfo.text)
        }
        IconButton(
            onClick = { onDelete() }, modifier = Modifier.padding(vertical = Base)
        ) {
            Icon(
                painter = rememberVectorPainter(image = Icons.Default.Delete),
                contentDescription = "Delete note",
            )
        }
    }
}