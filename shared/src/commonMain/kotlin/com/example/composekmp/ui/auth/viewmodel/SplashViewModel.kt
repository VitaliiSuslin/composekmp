package com.example.composekmp.ui.auth.viewmodel

import com.example.composekmp.auth.viewmodel.delegate.SplashUiEvent
import com.example.composekmp.auth.viewmodel.delegate.SplashViewModelDelegate
import com.example.composekmp.di.SharedInjector
import com.example.composekmp.util.util.KMPViewModel

internal class SplashViewModel(
    delegate: SplashViewModelDelegate = SharedInjector.splashViewModelDelegate
) : KMPViewModel(), SplashViewModelDelegate by delegate {
    init {
        delegate.initialize(viewModelScope)
        delegate.processUiEvent(SplashUiEvent.Start)
    }
}