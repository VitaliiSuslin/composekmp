package com.example.composekmp.ui.chooser

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.primarySurface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.composekmp.di.SharedInjector
import com.example.composekmp.getPlatform
import com.example.composekmp.settings.AppDb
import com.example.composekmp.ui.Screen
import com.example.composekmp.ui.navigateToNoteScreen
import com.example.composekmp.ui.navigateToSignInScreen
import com.example.composekmp.util.core.util.Level
import com.example.composekmp.view.Base
import com.example.composekmp.view.BaseSmallX2
import com.example.composekmp.view.CustomNavigation
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterialApi::class)
@Composable
internal fun ChooserUi(
    navController: CustomNavigation
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {


        val scope = rememberCoroutineScope()

        Card(
            modifier = Modifier.fillMaxWidth().padding(Base),
            border = BorderStroke(2.dp, MaterialTheme.colors.secondary),
            backgroundColor = MaterialTheme.colors.primarySurface,
            onClick = {
                scope.launch(getPlatform().dispatcherIO) {
                    SharedInjector.appSettings.setAppDbChoice(AppDb.REALM)
                }
                navController.navigateToNoteScreen(Screen.Chooser.route)
            }
        ) {
            Text(text = "Realm", modifier = Modifier.padding(BaseSmallX2))
        }

        Card(
            modifier = Modifier.fillMaxWidth().padding(Base),
            border = BorderStroke(2.dp, MaterialTheme.colors.primary),
            backgroundColor = MaterialTheme.colors.primaryVariant,
            onClick = {
                scope.launch(getPlatform().dispatcherIO) {
                    SharedInjector.appSettings.setAppDbChoice(AppDb.SQL_DELIGHT)
                }
                getPlatform().logger().log(Level.ERROR, navController.hashCode().toString())
                navController.navigateToNoteScreen(Screen.Chooser.route)
            }
        ) {
            Text(text = "SQDLight", modifier = Modifier.padding(BaseSmallX2))
        }
    }
}