package com.example.composekmp.ui

import com.example.composekmp.note.model.NoteInfo
import com.example.composekmp.util.core.util.ActionWrapper
import com.example.composekmp.util.core.util.receiveAsCleanableActionFlow
import com.example.composekmp.util.core.util.receiveMessagesCleanableFlow
import kotlin.time.Duration
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow

class MainUiProcessor {

    private val _showingDialogState = MutableStateFlow(DialogUiState())
    val showingDialogState = _showingDialogState.asStateFlow()

    private val _action = Channel<ActionWrapper<MainAction>>()
    val action: Flow<ActionWrapper<MainAction>> = _action.receiveAsCleanableActionFlow()
    private val _message = Channel<ActionWrapper.Message<MainMessage>>()
    val message: Flow<ActionWrapper<MainMessage>> = _message.receiveMessagesCleanableFlow()

    fun showNoteDialog(dialogUiState: DialogUiState) {
        _showingDialogState.value = dialogUiState.copy(isShowDialog = true)
    }

    fun showingDialog(boolean: Boolean) {
        if (boolean.not()) {
            _showingDialogState.value = DialogUiState()
        }
    }

    fun showMessage(message: String) {
        _message.trySend(ActionWrapper.Message(MainMessage.ShowMessagePopUp(message), 1000))
    }
}

data class DialogUiState(
    val noteInfo: NoteInfo? = null,
    val isShowDialog: Boolean = false,
    val setShowDialog: (Boolean) -> Unit = {},
    val onSave: (title: String, text: String) -> Unit = { _, _ -> }
)

sealed interface MainAction

sealed interface MainMessage {
    data class ShowMessagePopUp(val message: String) : MainMessage
}