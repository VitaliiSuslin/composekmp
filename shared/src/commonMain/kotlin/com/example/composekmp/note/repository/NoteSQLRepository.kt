package com.example.composekmp.note.repository

import com.example.composekmp.db.Note
import com.example.composekmp.getPlatform
import com.example.composekmp.note.model.CreateNote
import com.example.composekmp.note.model.NoteInfo
import com.example.composekmp.sqldelight.appDataBase
import com.example.composekmp.util.core.util.Level
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update

class NoteSQLRepository : NoteRepository {

    private val appDb by lazy { appDataBase() }

    private val dbEventBuss = MutableStateFlow<DbEvent>(DbEvent.Init())

    override suspend fun createNote(note: CreateNote) {
        runTransaction {
            appDb.database().kMPComposeDatabaseQueries.insertItem(
                getPlatform().generateUuid(),
                note.title,
                note.text
            )
        }
    }

    override suspend fun getNote(id: String): NoteInfo? {
        return runCatching {
            appDb.database().kMPComposeDatabaseQueries.selectByID(id).executeAsOne().toNoteInfo()
        }.getOrNull()
    }

    override suspend fun deleteNote(id: String) {
        getPlatform().logger().log(Level.ERROR, "Delete $id")
        appDb.database().kMPComposeDatabaseQueries.deleteItem(id)
        dbEventBuss.update { DbEvent.Update() }
    }

    override suspend fun updateNote(note: NoteInfo) {
        runTransaction {
            appDb.database().kMPComposeDatabaseQueries.insertItem(
                note.id,
                note.title,
                note.text
            )
        }
    }

    override fun observeAllNotes(): Flow<List<NoteInfo>> {
        return dbEventBuss
            .filter { it is DbEvent.Init || it is DbEvent.Update }
            .map {
                appDb.database().kMPComposeDatabaseQueries.selectAll().executeAsList()
                    .map { it.toNoteInfo() }
            }.onEach {
                getPlatform().logger().log(Level.ERROR, "observeAllNotes list $it")
            }
    }

    override fun searchNote(query: String): List<NoteInfo> {
        return appDb.database().kMPComposeDatabaseQueries.selectAll().executeAsList().filter {
            it.id.contains(query, ignoreCase = true)
                    || it.text.contains(query, ignoreCase = true)
                    || it.title.contains(query, ignoreCase = true)
        }.map { it.toNoteInfo() }
    }

    private fun Note.toNoteInfo(): NoteInfo = NoteInfo(id, title, text)

    private fun runTransaction(body: () -> Unit) {
        appDb.database().transaction {
            body()
            this.afterCommit { dbEventBuss.update { DbEvent.Update() } }
        }
    }

    internal sealed interface DbEvent {
        data class Init(
            private val id: String = getPlatform().generateUuid()
        ) : DbEvent

        data class Update(
            private val id: String = getPlatform().generateUuid()
        ) : DbEvent
    }
}