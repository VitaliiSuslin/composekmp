package com.example.composekmp.note.viewmodel.delegate

import com.example.composekmp.auth.repository.AuthRepository
import com.example.composekmp.getPlatform
import com.example.composekmp.note.model.CreateNote
import com.example.composekmp.note.model.NoteInfo
import com.example.composekmp.note.repository.NoteRepository
import com.example.composekmp.util.core.ui.ScreenState
import com.example.composekmp.util.core.util.ActionWrapper
import com.example.composekmp.util.core.util.FailureWrapper
import com.example.composekmp.util.core.util.receiveAsCleanableActionFlow
import com.example.composekmp.util.core.util.viewModelExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NoteViewModelDelegateImpl(
    private val authRepository: AuthRepository,
    private val noteRepository: NoteRepository
) : NoteViewModelDelegate {

    private lateinit var viewModelScope: CoroutineScope

    private val _isSearchMode = MutableStateFlow(false)
    override val isSearchMode: StateFlow<Boolean> = _isSearchMode.asStateFlow()

    private val _screenState = MutableStateFlow(ScreenState.LOADING)
    override val screenState: StateFlow<ScreenState> = _screenState.asStateFlow()

    private val _notes = MutableStateFlow<List<NoteInfo>>(emptyList())
    override val notes: StateFlow<List<NoteInfo>> = _notes.asStateFlow()

    private val _isUserLoggedIn = MutableStateFlow(false)
    override val isUserLoggedIn: StateFlow<Boolean> = _isUserLoggedIn.asStateFlow()

    private val _action = Channel<ActionWrapper<NoteAction>>()
    override val action: Flow<ActionWrapper<NoteAction>> = _action.receiveAsCleanableActionFlow()

    private val _error = Channel<FailureWrapper>()
    override val error: Flow<FailureWrapper> = _error.receiveAsFlow()

    private var originList = listOf<NoteInfo>()

    override fun initialize(viewModelScope: CoroutineScope) {
        this.viewModelScope = viewModelScope
    }

    override fun processUiEvent(uiEvent: NoteUiEvent) {
        viewModelScope.launch(
            viewModelExceptionHandler(
                errorChannel = _error,
                logger = getPlatform().logger()
            )
        ) {
            when (uiEvent) {
                NoteUiEvent.Start -> onStart()
                NoteUiEvent.SearchMode -> onSearchMode()
                NoteUiEvent.CloseSearchMode -> onCloseSearchMode()
                NoteUiEvent.LogOut -> onLogOut()
                is NoteUiEvent.AddNewNote -> onAddNewNote(uiEvent.createNote)
                is NoteUiEvent.UpdateNote -> onUpdateNote(uiEvent.noteInfo)
                is NoteUiEvent.DeleteNote -> onDeleteNote(uiEvent.noteInfo.id)
                is NoteUiEvent.SearchQuery -> onSearchQuery(uiEvent.query)
                NoteUiEvent.OpenCreateNote -> _action.send(ActionWrapper.Action(NoteAction.OpenNoteCreateDialog))
                is NoteUiEvent.OpenUpdateNote -> _action.send(
                    ActionWrapper.Action(NoteAction.OpenEditNoteDialog(uiEvent.noteInfo))
                )
                NoteUiEvent.LogIn -> _action.send(ActionWrapper.Action(NoteAction.OpenLogin))
            }
        }
    }

    //TODO Add closing and restart for this job
    private suspend fun onStart() = withContext(currentCoroutineContext()) {
        _isUserLoggedIn.value = authRepository.isUserLoggedIn()
        launch(getPlatform().dispatcherIO) {
            noteRepository.observeAllNotes().collect { notes ->
                originList = notes
                if (isSearchMode.value) {
                    return@collect
                }

                if (originList.isEmpty()) {
                    _screenState.value = ScreenState.PLACEHOLDER
                    _notes.value = emptyList()
                } else {
                    _screenState.value = ScreenState.CONTENT
                    _notes.value = originList
                }
            }
        }
    }

    private suspend fun onLogOut() {
        authRepository.signOut()
        _action.send(ActionWrapper.Action(NoteAction.MoteToSplash))
    }

    //FIXME: It's better to move logic into UseCase and use IO dispatcher according platform
    private suspend fun onAddNewNote(createNote: CreateNote) =
        withContext(getPlatform().dispatcherIO) {
            runCatching {
                noteRepository.createNote(createNote)
            }.onSuccess {
                _action.send(ActionWrapper.Action(NoteAction.NoteIsCreated))
            }.onFailure {
                it.printStackTrace()
                _error.send(FailureWrapper(it))
            }
        }

    //FIXME: It's better to move logic into UseCase and use IO dispatcher according platform
    private suspend fun onUpdateNote(noteInfo: NoteInfo) = withContext(getPlatform().dispatcherIO) {
        runCatching {
            noteRepository.updateNote(noteInfo)
        }.onSuccess {
            _action.send(ActionWrapper.Action(NoteAction.NoteIsUpdated))
        }.onFailure {
            it.printStackTrace()
            _error.send(FailureWrapper(it))
        }
    }

    //FIXME: It's better to move logic into UseCase and use IO dispatcher according platform
    private suspend fun onDeleteNote(id: String) = withContext(getPlatform().dispatcherIO) {
        runCatching {
            noteRepository.deleteNote(id)
        }.onSuccess {
            _action.send(ActionWrapper.Action(NoteAction.NoteIsDeleted))
        }.onFailure {
            it.printStackTrace()
            _error.send(FailureWrapper(it))
        }
    }

    private fun onSearchMode() {
        _isSearchMode.value = true
    }

    private fun onCloseSearchMode() {
        _isSearchMode.value = false
        if (originList.isEmpty()) {
            _screenState.value = ScreenState.PLACEHOLDER
        } else {
            _screenState.value = ScreenState.CONTENT
        }
        _notes.value = originList
    }

    //FIXME: It's better to move logic into UseCase and use IO dispatcher according platform
    private suspend fun onSearchQuery(query: String) = withContext(getPlatform().dispatcherIO) {
        runCatching {
            _screenState.value = ScreenState.LOADING
            noteRepository.searchNote(query)
        }.onSuccess {
            if (it.isEmpty()) {
                _screenState.value = ScreenState.PLACEHOLDER
            } else {
                _screenState.value = ScreenState.CONTENT
                _notes.value = it
            }
        }.onFailure {
            _screenState.value = ScreenState.PLACEHOLDER
            _error.send(FailureWrapper(it))
        }
    }

}