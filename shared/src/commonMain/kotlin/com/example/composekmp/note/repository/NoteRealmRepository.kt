package com.example.composekmp.note.repository

import com.example.composekmp.note.model.CreateNote
import com.example.composekmp.note.model.NoteInfo
import com.example.composekmp.realm.RealmModule
import com.example.composekmp.realm.model.Note
import io.realm.kotlin.UpdatePolicy
import io.realm.kotlin.ext.query
import io.realm.kotlin.types.RealmUUID
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class NoteRealmRepository(
    private val realmModule: RealmModule
) : NoteRepository {

    override suspend fun createNote(note: CreateNote) {
//        realmModule.invokeRealmAction {
//            this.writeBlocking {
//                copyToRealm(Note().apply {
//                    this.text = note.text
//                    this.title = note.title
//                })
//            }
//        }

        realmModule.getRealm().write {
            val newNote = Note().apply {
                this.text = note.text
                this.title = note.title
            }
            copyToRealm(newNote)
        }
    }

    override suspend fun getNote(id: String): NoteInfo? {
        return getRealmNote(id.toRealmUUID())?.toNoteInfo()
    }

    override suspend fun deleteNote(id: String) {
//        realmModule.invokeRealmAction {
//            getRealmNote(id.toRealmUUID())?.let { realmNote ->
//                this.writeBlocking {
//                    findLatest(realmNote)?.let {
//                        delete(it)
//                    }
//                }
//            }
//        }

        val realmNote = getRealmNote(id.toRealmUUID())
        if (realmNote != null) {
            realmModule.getRealm().write {
                val latest = findLatest(realmNote)
                if (latest != null) {
                    delete(latest)
                }
            }
        }

    }

    override suspend fun updateNote(note: NoteInfo) {
//        realmModule.invokeRealmAction {
//            writeBlocking {
//                copyToRealm(Note().apply {
//                    this._id = note.id.toRealmUUID()
//                    this.text = note.text
//                    this.title = note.title
//                }, UpdatePolicy.ALL)
//            }
//        }

        realmModule.getRealm().write {
            val newNote = Note().apply {
                this._id = note.id.toRealmUUID()
                this.text = note.text
                this.title = note.title
            }
            copyToRealm(newNote, UpdatePolicy.ALL)
        }
    }

    override fun observeAllNotes(): Flow<List<NoteInfo>> {
        return realmModule.getRealm()
            .query<Note>().asFlow()
            .map { noteResultsChange -> noteResultsChange.list.map { it.toNoteInfo() } }
    }

    override fun searchNote(query: String): List<NoteInfo> {
        return realmModule.getRealm()
            .query<Note>(
                query = "title = $0 OR text = $0",
                query
            )
            .find()
            .map { it.toNoteInfo() }

    }

    private fun getRealmNote(id: RealmUUID): Note? {
        return realmModule.getRealm()
            .query<Note>(
                query = "_id = $0",
                id,
            )
            .first()
            .find()
    }

    private fun Note.toNoteInfo() = NoteInfo(_id.toString(), title, text)
}

private fun String.toRealmUUID() = RealmUUID.from(this)