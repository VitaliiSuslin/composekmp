package com.example.composekmp.note.repository

import com.example.composekmp.note.model.CreateNote
import com.example.composekmp.note.model.NoteInfo
import io.realm.kotlin.types.RealmUUID
import kotlinx.coroutines.flow.Flow

interface NoteRepository {
    suspend fun createNote(note: CreateNote)
    suspend fun getNote(id: String): NoteInfo?
    suspend fun deleteNote(id: String)
    suspend fun updateNote(note: NoteInfo)
    fun observeAllNotes(): Flow<List<NoteInfo>>
    fun searchNote(query: String): List<NoteInfo>
}