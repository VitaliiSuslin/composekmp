package com.example.composekmp.note.viewmodel.delegate

import com.example.composekmp.note.model.CreateNote
import com.example.composekmp.note.model.NoteInfo
import com.example.composekmp.util.core.ui.ScreenState
import com.example.composekmp.util.core.util.ActionWrapper
import com.example.composekmp.util.core.util.FailureWrapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface NoteViewModelDelegate {
    val isSearchMode: StateFlow<Boolean>
    val screenState: StateFlow<ScreenState>
    val notes: StateFlow<List<NoteInfo>>

    val isUserLoggedIn: StateFlow<Boolean>

    val action: Flow<ActionWrapper<NoteAction>>
    val error: Flow<FailureWrapper>

    fun initialize(viewModelScope: CoroutineScope)
    fun processUiEvent(uiEvent: NoteUiEvent)
}

sealed interface NoteUiEvent {
    object Start : NoteUiEvent
    object SearchMode : NoteUiEvent
    object CloseSearchMode : NoteUiEvent
    object LogOut : NoteUiEvent
    object LogIn : NoteUiEvent
    object OpenCreateNote : NoteUiEvent
    data class OpenUpdateNote(val noteInfo: NoteInfo) : NoteUiEvent
    data class SearchQuery(val query: String) : NoteUiEvent
    data class AddNewNote(val createNote: CreateNote) : NoteUiEvent
    data class DeleteNote(val noteInfo: NoteInfo) : NoteUiEvent
    data class UpdateNote(val noteInfo: NoteInfo) : NoteUiEvent
}

sealed interface NoteAction {
    object MoteToSplash : NoteAction
    object NoteIsCreated : NoteAction
    object NoteIsUpdated : NoteAction
    object NoteIsDeleted : NoteAction

    object OpenNoteCreateDialog : NoteAction
    data class OpenEditNoteDialog(val noteInfo: NoteInfo) : NoteAction

    object OpenLogin : NoteAction
}