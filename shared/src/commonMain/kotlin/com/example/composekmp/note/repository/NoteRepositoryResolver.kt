package com.example.composekmp.note.repository

import com.example.composekmp.getPlatform
import com.example.composekmp.note.model.CreateNote
import com.example.composekmp.note.model.NoteInfo
import com.example.composekmp.realm.RealmModule
import com.example.composekmp.settings.AppDb
import com.example.composekmp.settings.AppSettings
import com.example.composekmp.settings.DEFAULT_DATABASE
import com.example.composekmp.util.core.util.Level
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.onEach

class NoteRepositoryResolver(
    private val realmModule: RealmModule,
    private val appSettings: AppSettings
) : NoteRepository {

    private var _selectedDb: AppDb = DEFAULT_DATABASE

    private var noteRepository: NoteRepository? = null

    override suspend fun createNote(note: CreateNote) = getRepository().createNote(note)

    override suspend fun getNote(id: String): NoteInfo? = getRepository().getNote(id)

    override suspend fun deleteNote(id: String) = getRepository().deleteNote(id)

    override suspend fun updateNote(note: NoteInfo) = getRepository().updateNote(note)

    override fun observeAllNotes(): Flow<List<NoteInfo>> =
        getRepository().observeAllNotes().onEach {
            getPlatform().logger().log(Level.ERROR, "observeAllNotes returns notes")
        }

    override fun searchNote(query: String): List<NoteInfo> = getRepository().searchNote(query)

    private fun getRepository(): NoteRepository {
        val appDb = appSettings.appDbChoice()
        return if (noteRepository != null && _selectedDb == appDb) {
            noteRepository!!
        } else {
            _selectedDb = appDb
            noteRepository = generateNoteRepository(appDb)
            noteRepository!!
        }
    }

    private fun generateNoteRepository(type: AppDb): NoteRepository {
        getPlatform().logger().log(Level.ERROR, "Db type $type")
        return when (type) {
            AppDb.REALM -> NoteRealmRepository(realmModule)
            AppDb.SQL_DELIGHT -> NoteSQLRepository()
        }
    }
}