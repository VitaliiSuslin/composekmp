package com.example.composekmp.note.model

data class NoteInfo(
    val id: String,
    val title: String,
    val text: String
)

data class CreateNote(
    val title: String,
    val text: String
)
