package com.example.composekmp.settings

import kotlinx.coroutines.flow.Flow

interface AppSettings {
    suspend fun setAppDbChoice(appDb: AppDb)
    fun appDbChoice(): AppDb
    fun observeAppDbChoice():Flow<AppDb>
}

enum class AppDb {
    REALM, SQL_DELIGHT
}

object PreferencesKey {
    const val APP_SETTINGS = "app_settings"
    const val APP_DB_TYPE = "app_db_type"
}

val DEFAULT_DATABASE = AppDb.SQL_DELIGHT