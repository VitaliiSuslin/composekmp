package com.example.composekmp.sqldelight

import com.example.composekmp.db.KMPComposeDatabase

interface SQLDelightPlatform {
    fun database(): KMPComposeDatabase
    fun closeConnection()
}

internal expect fun appDataBase(): SQLDelightPlatform
