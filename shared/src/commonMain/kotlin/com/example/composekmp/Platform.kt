package com.example.composekmp

import com.example.composekmp.settings.AppSettings
import com.example.composekmp.util.core.util.CustomLogger
import kotlinx.coroutines.CoroutineDispatcher

interface Platform {
    val name: String
    val dispatcherIO: CoroutineDispatcher
    val dispatcherMain: CoroutineDispatcher
    val platformType: PlatformType

    fun generateUuid(): String

    fun appSettings(): AppSettings

    fun logger(): CustomLogger
}

expect fun getPlatform(): Platform

enum class PlatformType {
    IOS, ANDROID
}