package com.example.composekmp.view

import androidx.compose.ui.unit.dp

val BaseSmallX2 = 10.dp
val BaseSmallX1 = 12.dp
val Base = 16.dp
val BaseBigX1 = 20.dp
val BaseBigX2 = 24.dp
val BaseBigX3 = 28.dp