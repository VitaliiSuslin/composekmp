package com.example.composekmp.view

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
internal fun SimpleProgress(modifier: Modifier = Modifier) {
    CircularProgressIndicator(
        modifier = modifier
            .width(BaseBigX1)
            .height(BaseBigX1)
    )
}
