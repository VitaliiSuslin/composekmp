package com.example.composekmp.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material.icons.rounded.Lock
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation


@Composable
internal fun SimpleTextField(
    getValue: @Composable () -> String,
    label: @Composable () -> String,
    isSingleLine: Boolean = true,
    onTextChanged: (String) -> Unit,
    onErrorChanged: @Composable () -> String? = { null },
    isEnable: @Composable () -> Boolean = { true },
    trailingIcon: @Composable (() -> Unit)? = null
) {
    val keyboard = LocalSoftwareKeyboardController.current
    val errorMessage = onErrorChanged()
    Column {
        TextField(
            value = getValue(),
            singleLine = isSingleLine,
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(onDone = { keyboard?.hide() }),
            modifier = Modifier
                .fillMaxWidth(),
            onValueChange = onTextChanged,
            label = {
                Text(text = label())
            },
            isError = errorMessage != null,
            enabled = isEnable(),
            trailingIcon = trailingIcon
        )

        errorMessage?.let {
            Text(
                text = it,
                color = MaterialTheme.colors.onError,
                style = MaterialTheme.typography.subtitle2,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = BaseBigX2, end = BaseBigX2)
            )
        }
    }
}


@Composable
internal fun PasswordTextField(
    value: @Composable () -> String,
    label: @Composable () -> String,
    onTextChanged: (String) -> Unit,
    onErrorChanged: @Composable () -> String? = { null },
    isEnable: @Composable () -> Boolean = { true }
) {
    val passwordVisibility = remember { mutableStateOf(false) }
    val keyboard = LocalSoftwareKeyboardController.current
    val errorMessage = onErrorChanged()

    Column {
        TextField(
            value = value(),
            singleLine = true,
            modifier = Modifier
                .fillMaxWidth(),
            visualTransformation = if (passwordVisibility.value) VisualTransformation.None else PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(onDone = {
                keyboard?.hide()
            }),
            trailingIcon = {
                val icon =
                    if (passwordVisibility.value) Icons.Default.Warning
                    else Icons.Rounded.Lock
                IconButton(onClick = {
                    passwordVisibility.value = !passwordVisibility.value
                }) {
                    Icon(painter = rememberVectorPainter(image = icon), contentDescription = null)
                }
            },
            onValueChange = onTextChanged,
            label = {
                Text(text = label())
            },
            isError = errorMessage != null,
            enabled = isEnable()
        )

        errorMessage?.let {
            Text(
                text = it,
                color = MaterialTheme.colors.onError,
                style = MaterialTheme.typography.subtitle2,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = BaseBigX2, end = BaseBigX2)
            )
        }
    }
}