package com.example.composekmp.view

//TODO find solution how to create stack bundle with arguments for CustomNavigation
class CustomNavigation(
    private val startDestination: String,
    private val onNavigationListener: (closableRoute: String) -> Unit = {}
) {

    private val navigationRoutesQueue by lazy { mutableListOf<String>() }
    private val navigationComposableList by lazy { mutableMapOf<String, () -> Unit>() }


    fun navigate(route: String) {
        if (!navigationComposableList.containsKey(route)) return

        navigationRoutesQueue.add(route)
        navigationComposableList[route]?.invoke()
    }

    fun navigate(route: String, customNavigationSettings: () -> CustomNavigationSettings) {
        if (!navigationComposableList.containsKey(route)) return
        val builder = customNavigationSettings()
        if (builder.inclusive) {
            cleanRoutes()
        }

        navigationRoutesQueue.add(route)
        navigationComposableList[route]?.invoke()
    }


    fun popBackStack() {
        val currentRouteIndex = navigationRoutesQueue.lastIndex
        if (currentRouteIndex <= 0) {
            return
        }

        val currentRoute = navigationRoutesQueue[currentRouteIndex]
        navigationRoutesQueue.remove(currentRoute)

        val lastRoute = navigationRoutesQueue.last()
        navigationComposableList[lastRoute]?.invoke()
    }

    fun composable(route: String, onRouteDetected: () -> Unit) {
        navigationComposableList[route] = onRouteDetected
    }

    fun start() {
        navigate(startDestination)
    }

    private fun cleanRoutes() {
        navigationRoutesQueue.forEach {
            onNavigationListener(it)
        }
        navigationRoutesQueue.clear()
    }
}

class CustomNavigationSettings {

    var popUpRoute: String? = null

    /**
     * Whether the `popUpTo` destination should be popped from the back stack.
     */
    var inclusive: Boolean = false

    /**
     * The back stack and the state of all destinations between the current destination
     * and the popUpTo ID should be saved for later restoration
     * */
    var saveState: Boolean = false
}