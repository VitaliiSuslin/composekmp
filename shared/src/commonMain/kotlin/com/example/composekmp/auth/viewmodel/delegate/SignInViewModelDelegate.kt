package com.example.composekmp.auth.viewmodel.delegate

import com.example.composekmp.settings.AppDb
import com.example.composekmp.util.core.util.ActionWrapper
import com.example.composekmp.util.core.util.FailureWrapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface SignInViewModelDelegate {

    val isSystemProcessing: StateFlow<Boolean>

    val email: StateFlow<String>
    val emailError: StateFlow<Throwable?>

    val password: StateFlow<String>
    val passwordError: StateFlow<Throwable?>

    val dbState: StateFlow<AppDb>

    val action: Flow<ActionWrapper<SignInActions>>
    val error: Flow<FailureWrapper>

    fun processUiEvent(uiEvent: SignInUiEvent)
    fun initialize(viewModelScope: CoroutineScope)
}

sealed interface SignInUiEvent {
    object Start: SignInUiEvent
    object SignIn : SignInUiEvent
    data class EmailIsChanged(val email: String) : SignInUiEvent
    data class PasswordIsChanged(val password: String) : SignInUiEvent
    data class AppDbIsChanged(val appDb: AppDb) : SignInUiEvent
}

sealed interface SignInActions {
    object OpenNotesScreen : SignInActions
}