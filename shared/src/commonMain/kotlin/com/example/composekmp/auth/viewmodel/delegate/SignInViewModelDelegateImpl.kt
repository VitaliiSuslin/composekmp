package com.example.composekmp.auth.viewmodel.delegate

import com.example.composekmp.auth.model.SignIn
import com.example.composekmp.auth.repository.AuthRepository
import com.example.composekmp.getPlatform
import com.example.composekmp.settings.AppDb
import com.example.composekmp.settings.AppSettings
import com.example.composekmp.util.core.ui.isEmailNotValid
import com.example.composekmp.util.core.ui.isPasswordNotValid
import com.example.composekmp.util.core.util.ActionWrapper
import com.example.composekmp.util.core.util.FailureWrapper
import com.example.composekmp.util.core.util.receiveAsCleanableActionFlow
import com.example.composekmp.util.core.util.viewModelExceptionHandler
import kotlin.coroutines.coroutineContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SignInViewModelDelegateImpl(
    private val authRepository: AuthRepository,
    private val appSettings: AppSettings
) : SignInViewModelDelegate {

    private lateinit var viewModelScope: CoroutineScope

    private val _isSystemProcessing = MutableStateFlow(false)
    override val isSystemProcessing: StateFlow<Boolean> = _isSystemProcessing.asStateFlow()

    private val _email = MutableStateFlow(String())
    override val email: StateFlow<String> = _email.asStateFlow()
    private val _emailError = MutableStateFlow<Throwable?>(null)
    override val emailError: StateFlow<Throwable?> = _emailError.asStateFlow()

    private val _password = MutableStateFlow(String())
    override val password: StateFlow<String> = _password.asStateFlow()
    private val _passwordError = MutableStateFlow<Throwable?>(null)
    override val passwordError: StateFlow<Throwable?> = _passwordError.asStateFlow()

    private val _dbState = MutableStateFlow(AppDb.REALM)
    override val dbState: StateFlow<AppDb> = _dbState.asStateFlow()

    private val _action = Channel<ActionWrapper<SignInActions>>()
    override val action: Flow<ActionWrapper<SignInActions>> = _action.receiveAsCleanableActionFlow()

    private val _error = Channel<FailureWrapper>()
    override val error: Flow<FailureWrapper> = _error.receiveAsFlow()

    override fun initialize(viewModelScope: CoroutineScope) {
        this.viewModelScope = viewModelScope
    }

    override fun processUiEvent(uiEvent: SignInUiEvent) {
        viewModelScope.launch(
            viewModelExceptionHandler(
                errorChannel = _error,
                logger = getPlatform().logger()
            )
        ) {
            when (uiEvent) {
                SignInUiEvent.Start -> {
                    _dbState.value = appSettings.appDbChoice()
                    launch {
                        observeAppDbChoice()
                    }
                }
                is SignInUiEvent.EmailIsChanged -> {
                    emailError.value?.let {
                        _emailError.value = null
                    }
                    _email.value = uiEvent.email
                }
                is SignInUiEvent.PasswordIsChanged -> {
                    passwordError.value?.let {
                        _passwordError.value = null
                    }
                    _password.value = uiEvent.password
                }
                SignInUiEvent.SignIn -> signIn()
                is SignInUiEvent.AppDbIsChanged -> {
                    appSettings.setAppDbChoice(uiEvent.appDb)
                }
            }
        }
    }

    private suspend fun observeAppDbChoice() {
        appSettings.observeAppDbChoice().collect { newAppDb ->
            _dbState.value = newAppDb
        }
    }

    private suspend fun signIn() = withContext(coroutineContext) {
        systemProcessing()
        val email = email.value
        if (email.isBlank() || isEmailNotValid(email)) {
            _emailError.value = Exception("Email is empty or not valid!")
        }
        val password = password.value
        if (password.isBlank() || isPasswordNotValid(password)) {
            _passwordError.value = Exception("Password is empty or not valid")
        }

        if (emailError.value != null || passwordError.value != null) {
            systemStopProcessing()
            return@withContext
        }

        runCatching {
            authRepository.signIn(SignIn(email, password))
            defaultState()
        }.onSuccess {
            _action.send(ActionWrapper.Action(SignInActions.OpenNotesScreen))
        }.onFailure {
            _error.send(FailureWrapper(it))
        }
        systemStopProcessing()
    }

    private fun systemProcessing() {
        _isSystemProcessing.value = true
    }

    private fun systemStopProcessing() {
        _isSystemProcessing.value = false
    }

    private fun defaultState() {
        _email.value = String()
        _emailError.value = null
        _password.value = String()
        _passwordError.value = null
    }
}