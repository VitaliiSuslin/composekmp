package com.example.composekmp.auth.viewmodel.delegate

import com.example.composekmp.auth.repository.AuthRepository
import com.example.composekmp.getPlatform
import com.example.composekmp.util.core.util.ActionWrapper
import com.example.composekmp.util.core.util.FailureWrapper
import com.example.composekmp.util.core.util.receiveAsCleanableActionFlow
import com.example.composekmp.util.core.util.viewModelExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class SplashViewModelDelegateImpl(
    private val authRepository: AuthRepository
) : SplashViewModelDelegate {

    private lateinit var viewModelScope: CoroutineScope

    private val _action = Channel<ActionWrapper<SplashAction>>()
    override val action: Flow<ActionWrapper<SplashAction>> = _action.receiveAsCleanableActionFlow()

    private val _error = Channel<FailureWrapper>()
    override val error: Flow<FailureWrapper> = _error.receiveAsFlow()

    override fun initialize(viewModelScope: CoroutineScope) {
        this.viewModelScope = viewModelScope
    }

    override fun processUiEvent(uiEvent: SplashUiEvent) {
        viewModelScope.launch(
            viewModelExceptionHandler(
                errorChannel = _error,
                logger = getPlatform().logger()
            )
        ) {
            when (uiEvent) {
                SplashUiEvent.Start -> {
                    delay(1000L)
                    if (authRepository.isUserLoggedIn()) {
                        _action.send(ActionWrapper.Action(SplashAction.OpenNotesScreen))
                    } else {
                        _action.send(ActionWrapper.Action(SplashAction.OpenChooserScreen))
                    }
                }
            }
        }
    }
}