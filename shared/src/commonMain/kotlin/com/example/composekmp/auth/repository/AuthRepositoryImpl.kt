package com.example.composekmp.auth.repository

import com.example.composekmp.auth.model.SignIn
import com.example.composekmp.auth.model.SignUp
import com.example.composekmp.realm.RealmModule
import io.realm.kotlin.mongodb.User

class AuthRepositoryImpl(
    private val realmModule: RealmModule
) : AuthRepository {
    override suspend fun signIn(signIn: SignIn) {
        realmModule.signIn(signIn.email, signIn.password)
    }

    override suspend fun signUp(signIn: SignUp) {
        realmModule.signUp(signIn.email, signIn.password)
    }

    override suspend fun signOut() {
        realmModule.signOut()
    }

    override suspend fun isUserLoggedIn(): Boolean {
        return realmModule.getCurrentUser()?.state == User.State.LOGGED_IN
    }
}