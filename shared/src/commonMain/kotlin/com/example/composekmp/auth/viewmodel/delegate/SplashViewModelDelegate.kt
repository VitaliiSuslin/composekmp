package com.example.composekmp.auth.viewmodel.delegate

import com.example.composekmp.util.core.util.ActionWrapper
import com.example.composekmp.util.core.util.FailureWrapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

interface SplashViewModelDelegate {

    val action: Flow<ActionWrapper<SplashAction>>
    val error: Flow<FailureWrapper>

    fun initialize(viewModelScope: CoroutineScope)
    fun processUiEvent(uiEvent: SplashUiEvent)
}

sealed interface SplashUiEvent {
    object Start : SplashUiEvent
}

sealed interface SplashAction {
    object OpenChooserScreen : SplashAction
    object OpenNotesScreen : SplashAction
}