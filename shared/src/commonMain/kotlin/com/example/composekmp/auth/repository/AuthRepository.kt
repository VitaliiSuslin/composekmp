package com.example.composekmp.auth.repository

import com.example.composekmp.auth.model.SignIn
import com.example.composekmp.auth.model.SignUp

interface AuthRepository {
    suspend fun signIn(signIn: SignIn)
    suspend fun signUp(signIn: SignUp)
    suspend fun signOut()
    suspend fun isUserLoggedIn(): Boolean
}