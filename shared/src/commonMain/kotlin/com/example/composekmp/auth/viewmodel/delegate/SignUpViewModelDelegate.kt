package com.example.composekmp.auth.viewmodel.delegate

import com.example.composekmp.util.core.util.ActionWrapper
import com.example.composekmp.util.core.util.FailureWrapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface SignUpViewModelDelegate {
    val isSystemProcessing: StateFlow<Boolean>

    val email: StateFlow<String>
    val emailError: StateFlow<Throwable?>

    val password: StateFlow<String>
    val passwordError: StateFlow<Throwable?>

    val action: Flow<ActionWrapper<SignUpAction>>
    val error: Flow<FailureWrapper>

    fun processUiEvent(uiEvent: SignUpUiEvent)
    fun initialize(viewModelScope: CoroutineScope)
}

sealed interface SignUpUiEvent {
    object SignUp : SignUpUiEvent
    data class EmailIsChanged(val email: String) : SignUpUiEvent
    data class PasswordIsChanged(val password: String) : SignUpUiEvent

    object RemoveEmailError : SignUpUiEvent
    object RemovePasswordError : SignUpUiEvent
}

sealed interface SignUpAction {
    object OpenNotesScreen : SignUpAction
}