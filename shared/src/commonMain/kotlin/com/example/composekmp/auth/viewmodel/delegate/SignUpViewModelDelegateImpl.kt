package com.example.composekmp.auth.viewmodel.delegate

import com.example.composekmp.auth.model.SignIn
import com.example.composekmp.auth.model.SignUp
import com.example.composekmp.auth.repository.AuthRepository
import com.example.composekmp.getPlatform
import com.example.composekmp.util.core.ui.isEmailNotValid
import com.example.composekmp.util.core.ui.isPasswordNotValid
import com.example.composekmp.util.core.util.ActionWrapper
import com.example.composekmp.util.core.util.FailureWrapper
import com.example.composekmp.util.core.util.viewModelExceptionHandler
import kotlin.coroutines.coroutineContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class SignUpViewModelDelegateImpl(
    private val authRepository: AuthRepository
) : SignUpViewModelDelegate {
    private lateinit var viewModelScope: CoroutineScope

    private val _isSystemProcessing = MutableStateFlow(false)
    override val isSystemProcessing: StateFlow<Boolean> = _isSystemProcessing.asStateFlow()

    private val _email = MutableStateFlow(String())
    override val email: StateFlow<String> = _email.asStateFlow()
    private val _emailError = MutableStateFlow<Throwable?>(null)
    override val emailError: StateFlow<Throwable?> = _emailError.asStateFlow()

    private val _password = MutableStateFlow(String())
    override val password: StateFlow<String> = _password.asStateFlow()

    private val _passwordError = MutableStateFlow<Throwable?>(null)
    override val passwordError: StateFlow<Throwable?> = _passwordError.asStateFlow()

    private val _action = Channel<ActionWrapper<SignUpAction>>()
    override val action: Flow<ActionWrapper<SignUpAction>> = _action.receiveAsFlow()

    private val _error = Channel<FailureWrapper>()
    override val error: Flow<FailureWrapper> = _error.receiveAsFlow()

    override fun initialize(viewModelScope: CoroutineScope) {
        this.viewModelScope = viewModelScope
    }

    override fun processUiEvent(uiEvent: SignUpUiEvent) {
        viewModelScope.launch(
            viewModelExceptionHandler(
                errorChannel = _error,
                logger = getPlatform().logger()
            )
        ) {
            when (uiEvent) {
                is SignUpUiEvent.EmailIsChanged -> _email.value = uiEvent.email
                is SignUpUiEvent.PasswordIsChanged -> _password.value = uiEvent.password
                SignUpUiEvent.RemoveEmailError -> _emailError.value = null
                SignUpUiEvent.RemovePasswordError -> _passwordError.value = null
                SignUpUiEvent.SignUp -> signUp()
            }
        }
    }

    private suspend fun signUp() = withContext(coroutineContext) {
        systemProcessing()
        val email = email.value
        if (email.isBlank() || isEmailNotValid(email)) {
            _emailError.value = Exception("Email is empty or not valid!")
        }
        val password = password.value
        if (password.isBlank() || isPasswordNotValid(password)) {
            _passwordError.value = Exception("Password is empty or not valid")
        }

        if (emailError.value != null || passwordError.value != null) {
            systemStopProcessing()
            return@withContext
        }

        runCatching {
            authRepository.signUp(SignUp(email, password))
            authRepository.signIn(SignIn(email, password))
        }.onSuccess {
            _action.send(ActionWrapper.Action(SignUpAction.OpenNotesScreen))
        }.onFailure {
            _error.send(FailureWrapper(it))
            systemStopProcessing()
        }
    }

    private fun systemProcessing() {
        _isSystemProcessing.value = true
    }

    private fun systemStopProcessing() {
        _isSystemProcessing.value = false
    }

}