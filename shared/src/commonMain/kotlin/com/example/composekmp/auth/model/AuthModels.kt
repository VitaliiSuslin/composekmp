package com.example.composekmp.auth.model

data class SignIn(
    val email: String,
    val password: String
)

data class SignUp(
    val email: String,
    val password: String
)