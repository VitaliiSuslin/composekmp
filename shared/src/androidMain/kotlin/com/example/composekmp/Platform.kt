package com.example.composekmp

import android.content.Context
import com.example.composekmp.logger.AndroidLogger
import com.example.composekmp.settings.AndroidAppSettings
import com.example.composekmp.settings.AppSettings
import com.example.composekmp.util.core.util.CustomLogger
import java.util.UUID
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import org.koin.core.component.KoinComponent

class AndroidPlatform : Platform, KoinComponent {
    override val name: String = "Android ${android.os.Build.VERSION.SDK_INT}"
    override val dispatcherIO: CoroutineDispatcher get() = Dispatchers.IO
    override fun generateUuid(): String = UUID.randomUUID().toString()
    override fun appSettings(): AppSettings {
        val context = getKoin().get<Context>()
        return AndroidAppSettings(context)
    }

    override val dispatcherMain: CoroutineDispatcher get() = Dispatchers.Main.immediate

    override val platformType: PlatformType = PlatformType.ANDROID

    override fun logger(): CustomLogger = AndroidLogger()
}

actual fun getPlatform(): Platform = AndroidPlatform()