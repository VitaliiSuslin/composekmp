package com.example.composekmp.sqldelight

import android.app.Application
import app.cash.sqldelight.driver.android.AndroidSqliteDriver
import com.example.composekmp.db.KMPComposeDatabase
import org.koin.core.component.KoinComponent

class AndroidSqlDelight : SQLDelightPlatform, KoinComponent {

    private val applicationContext: Application = getKoin().get()

    private val driver by lazy {
        AndroidSqliteDriver(KMPComposeDatabase.Schema, applicationContext, SQLDelightConstant.DB_NAME)
    }

    //TODO think about getting platform driver, not all KMPComposeDatabase instead
    override fun database(): KMPComposeDatabase {
        return KMPComposeDatabase(driver)
    }

    override fun closeConnection() {
        driver.close()
    }
}

actual fun appDataBase(): SQLDelightPlatform = AndroidSqlDelight()
