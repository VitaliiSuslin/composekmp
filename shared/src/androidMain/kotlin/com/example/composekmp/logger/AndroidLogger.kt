package com.example.composekmp.logger

import android.util.Log
import com.example.composekmp.util.core.util.CustomLogger
import com.example.composekmp.util.core.util.Level

class AndroidLogger : CustomLogger {
    companion object {
        private const val TAG = "Logger"
    }

    override fun log(throwable: Throwable) {
        Log.e(TAG, "Error: ", throwable)
    }

    override fun log(level: Level, throwable: Throwable) {
        when (level) {
            Level.ERROR -> Log.e(TAG, Level.ERROR.name, throwable)
            Level.WARNING -> Log.w(TAG, Level.WARNING.name, throwable)
            Level.DEBUG -> Log.d(TAG, Level.WARNING.name, throwable)
            Level.INFORMATION -> Log.i(TAG, Level.WARNING.name, throwable)
            Level.VERBOSE -> Log.v(TAG, Level.WARNING.name, throwable)
        }

    }

    override fun log(level: Level, throwable: Throwable, vararg message: String) {
        when (level) {
            Level.ERROR -> Log.e(TAG, message.toString(), throwable)
            Level.WARNING -> Log.w(TAG, message.toString(), throwable)
            Level.DEBUG -> Log.d(TAG, message.toString(), throwable)
            Level.INFORMATION -> Log.i(TAG, message.toString(), throwable)
            Level.VERBOSE -> Log.v(TAG, message.toString(), throwable)
        }
    }

    override fun log(level: Level, vararg message: String) {
        val text = message.joinToString()
        when (level) {
            Level.ERROR -> Log.e(TAG, text)
            Level.WARNING -> Log.w(TAG, text)
            Level.DEBUG -> Log.d(TAG, text)
            Level.INFORMATION -> Log.i(TAG, text)
            Level.VERBOSE -> Log.v(TAG, text)
        }
    }
}