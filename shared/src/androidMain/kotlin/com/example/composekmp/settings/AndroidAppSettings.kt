package com.example.composekmp.settings

import android.content.Context
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.receiveAsFlow

class AndroidAppSettings(private val context: Context) : AppSettings {

    private val appStateChoice = Channel<AppDb>()

    override suspend fun setAppDbChoice(appDb: AppDb) {
        appStateChoice.send(appDb)
        context.getSharedPreferences(PreferencesKey.APP_SETTINGS, Context.MODE_PRIVATE)
            .edit()
            .putString(PreferencesKey.APP_DB_TYPE, appDb.name)
            .apply()
    }

    override fun appDbChoice(): AppDb {
        return context.getSharedPreferences(PreferencesKey.APP_SETTINGS, Context.MODE_PRIVATE)
            .getString(PreferencesKey.APP_DB_TYPE, null)?.let {
                AppDb.valueOf(it)
            } ?: DEFAULT_DATABASE
    }

    override fun observeAppDbChoice(): Flow<AppDb> {
        return appStateChoice.receiveAsFlow()
    }
}