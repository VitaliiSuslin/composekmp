package com.example.composekmp.ui

import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.window.Dialog
import com.example.composekmp.di.UiInjector
import com.example.composekmp.util.core.util.ActionWrapper
import com.example.composekmp.view.Base
import com.example.composekmp.view.BaseSmallX1
import com.example.composekmp.view.BaseSmallX2
import com.example.composekmp.view.SimpleTextField
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

@ExperimentalComposeUiApi
@Composable
fun MainScreen(modifier: Modifier = Modifier) {

    val mainUiProcessor = UiInjector.mainProcessor

    Box(modifier = modifier) {
        ObserveDialog(mainUiProcessor.showingDialogState)
        MainUi(modifier = modifier)
    }

    ObserveMainActions(mainUiProcessor.action)
    ObserveMainMessages(mainUiProcessor.message)
}

@ExperimentalComposeUiApi
@Composable
private fun ObserveDialog(dialogState: StateFlow<DialogUiState>) {
    val dialog = dialogState.collectAsState().value

    if (dialog.isShowDialog) {
        NoteDialog(dialog)
    }
}

@Composable
private fun ObserveMainActions(action: Flow<ActionWrapper<MainAction>>) {
    action.collectAsState(ActionWrapper.Empty()).value.getContentIfNotConsumed()?.let { _ -> }
}

@Composable
private fun ObserveMainMessages(action: Flow<ActionWrapper<MainMessage>>) {
    action.collectAsState(ActionWrapper.Empty()).value.getContentIfNotConsumed()?.let { _action ->
        when (_action) {
            is MainMessage.ShowMessagePopUp -> Toast.makeText(
                LocalContext.current,
                _action.message,
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}


@ExperimentalComposeUiApi
@Composable
private fun NoteDialog(
    dialogUiState: DialogUiState
) {
    val titleState = remember { mutableStateOf(dialogUiState.noteInfo?.title ?: "") }
    val titleError = remember { mutableStateOf<String?>(null) }

    val textState = remember { mutableStateOf(dialogUiState.noteInfo?.text ?: "") }
    val textError = remember { mutableStateOf<String?>(null) }

    Dialog(
        onDismissRequest = {
            dialogUiState.setShowDialog(false)
        }
    ) {
        Surface(
            color = MaterialTheme.colors.background,
            shape = RoundedCornerShape(BaseSmallX2),
        ) {
            Column(modifier = Modifier.padding(BaseSmallX2)) {
                Spacer(modifier = Modifier.height(Base))
                SimpleTextField(
                    getValue = { titleState.value },
                    label = { "Title" },
                    onTextChanged = {
                        titleState.value = it
                        titleError.value?.let {
                            titleError.value = null
                        }
                    },
                    onErrorChanged = { titleError.value }
                )
                Spacer(modifier = Modifier.height(BaseSmallX1))
                SimpleTextField(
                    getValue = { textState.value },
                    label = { "Text" },
                    onTextChanged = {
                        textState.value = it
                        textError.value?.let {
                            textError.value = null
                        }
                    },
                    onErrorChanged = { textError.value }
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(Base),
                    horizontalArrangement = Arrangement.End
                ) {
                    Button(onClick = {
                        val newTitle = titleState.value
                        if (newTitle.isBlank()) {
                            titleError.value = "Empty field!"
                        }
                        val newText = textState.value
                        if (newText.isBlank()) {
                            textError.value = "Empty field!"
                        }

                        if (titleError.value == null && textError.value == null) {
                            dialogUiState.onSave(newTitle, newText)
                            dialogUiState.setShowDialog(false)
                        }

                    }) {
                        Text(text = "Save")
                    }
                    Spacer(modifier = Modifier.width(BaseSmallX1))
                    Button(onClick = {
                        dialogUiState.setShowDialog(false)
                    }) {
                        Text(text = "Cancel")
                    }
                    Spacer(modifier = Modifier.width(BaseSmallX1))
                }
            }
        }
    }
}