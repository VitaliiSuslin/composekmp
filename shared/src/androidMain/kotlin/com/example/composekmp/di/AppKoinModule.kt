package com.example.composekmp.di

import android.app.Application
import com.example.composekmp.ui.auth.viewmodel.SignInViewModel
import com.example.composekmp.ui.auth.viewmodel.SignUpViewModel
import com.example.composekmp.ui.auth.viewmodel.SplashViewModel
import com.example.composekmp.ui.note.viewmodel.NoteViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class AppKoinModule {

    companion object {
        fun init(application: Application) {
            val androidModule = AppKoinModule().instance
            startKoin {
                androidContext(application)
                modules(androidModule)
            }
        }
    }

    val instance = module {
        factory { SharedInjector.noteViewModelDelegate }
    }
}