plugins {
    kotlin("multiplatform")
    id("com.android.library")
    id("org.jetbrains.compose")
    id("io.realm.kotlin") version "1.6.0"
    id("app.cash.sqldelight")
}

kotlin {
    android()

    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach {
        it.binaries.framework {
            baseName = "shared"
            isStatic = true
            freeCompilerArgs += listOf(
                "-linker-option", "-framework", "-linker-option", "Metal",
                "-linker-option", "-framework", "-linker-option", "CoreText",
                "-linker-option", "-framework", "-linker-option", "CoreGraphics",
                "-Xdisable-phases=VerifyBitcode"
            )
        }
    }

    targets.withType<org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget> {
        binaries.withType<org.jetbrains.kotlin.gradle.plugin.mpp.Framework> {
            linkerOpts.add("-lsqlite3")
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                //Important for Kotlin/Compose multiplatform
                implementation("org.jetbrains.kotlinx:atomicfu:0.18.3")

                //Realm
                implementation("io.realm.kotlin:library-sync:1.6.0")
                //SQLDelight
                implementation(SqlDelight.runtime)
                implementation(SqlDelight.primitive)

                implementation(compose.ui)
                implementation(compose.foundation)
                implementation(compose.material)
                implementation(compose.runtime)
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val androidMain by getting {
            dependsOn(commonMain)
            dependencies {
                implementation(compose.preview)
                implementation(SqlDelight.android)

                val koinVersion = "3.3.2"
                val koinComposeVersion = "3.4.1"

                implementation("io.insert-koin:koin-android:$koinVersion")
                implementation("io.insert-koin:koin-android-compat:${koinVersion}")
                implementation("io.insert-koin:koin-androidx-compose:$koinComposeVersion")

                implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.5.1")
                implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.5.1")
            }
        }

        val iosX64Main by getting
        val iosArm64Main by getting
        val iosSimulatorArm64Main by getting
        val iosMain by creating {
            dependsOn(commonMain)
            dependencies {
                implementation(compose.ui)
                implementation(SqlDelight.native)
            }
            iosX64Main.dependsOn(this)
            iosArm64Main.dependsOn(this)
            iosSimulatorArm64Main.dependsOn(this)
        }
        val iosX64Test by getting
        val iosArm64Test by getting
        val iosSimulatorArm64Test by getting
        val iosTest by creating {
            dependsOn(commonTest)
            iosX64Test.dependsOn(this)
            iosArm64Test.dependsOn(this)
            iosSimulatorArm64Test.dependsOn(this)
        }
    }

    // Required for iOS build
    sourceSets.all {
        languageSettings.apply {
            optIn("kotlin.RequiresOptIn")
            optIn("kotlinx.coroutines.ExperimentalCoroutinesApi")
            optIn("androidx.compose.ui.ExperimentalComposeUiApi")
        }
    }
}

android {
    namespace = "com.example.composekmp"
    compileSdk = 33
    defaultConfig {
        minSdk = 26
        targetSdk = 33
    }
}

sqldelight {

    databases {
        create("KMPComposeDatabase") {
            packageName.set("com.example.composekmp.db")
//            sourceFolders
//            sourceFolders = listOf("sqldelight")
        }
    }
    linkSqlite.set(true)
}

kotlin {
    targets.withType<org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget> {
        binaries.all {
            // TODO: the current compose binary surprises LLVM, so disable checks for now.
            freeCompilerArgs += "-Xdisable-phases=VerifyBitcode"
        }
    }
}

object SqlDelight {
    //    const val runtime = "com.squareup.sqldelight:runtime:1.5.4"
    const val runtime = "app.cash.sqldelight:runtime:2.0.0-alpha05"

    //    const val android = "com.squareup.sqldelight:android-driver:1.5.4"
    const val android = "app.cash.sqldelight:android-driver:2.0.0-alpha05"

    //    const val native = "com.squareup.sqldelight:native-driver:1.5.4"
    const val native = "app.cash.sqldelight:native-driver:2.0.0-alpha05"

    //    primitive adapter
    const val primitive = "app.cash.sqldelight:primitive-adapters:2.0.0-alpha05"
}
