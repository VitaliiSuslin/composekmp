plugins {
    //trick: for the same plugin versions in all sub-modules
    id("com.android.application").version("7.3.1").apply(false)
    id("com.android.library").version("7.3.1").apply(false)
    kotlin("android").version("1.8.0").apply(false)
    kotlin("multiplatform").version("1.8.0").apply(false)
    id("org.jetbrains.compose").version("1.3.0").apply(false)
    id("app.cash.sqldelight").version("2.0.0-alpha05").apply(false)
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}

buildscript {
    dependencies {
        classpath("io.realm.kotlin:library-sync:1.6.0")
//        classpath("io.realm.kotlin:library-base:1.6.0")
    }

    repositories {
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}